﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Oefening_23._3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        List<Werknemer> werknemers = new List<Werknemer>();
        bool isleeg = true;
        BitmapImage geslacht = new BitmapImage();
        private void btnToevoegen_Click(object sender, RoutedEventArgs e)
        {
            decimal loon;
            int aantal;
            decimal commissie;
            
            try
            {
                if (decimal.TryParse(txtLoon.Text, out loon))
                {
                    //if (rdoWerknemer.IsChecked == true)
                    //{
                    //    Werknemer werknemer = new Werknemer(txtAchternaam.Text, txtVoornaam.Text, loon, geslacht );
                    //    VoegWerknemerToe(werknemer);
                    //}
                     if (rdoStukWerker.IsChecked == true)
                    {
                        if (int.TryParse(txtAantal.Text, out aantal))
                        {
                            StukWerker stukWerker = new StukWerker(txtAchternaam.Text, txtVoornaam.Text, loon, aantal, geslacht);
                            VoegWerknemerToe(stukWerker);
                        }
                        else
                        {
                            throw new FormatException("Het aantal moet een geheel getal zijn!");
                        }
                    }
                    else if (rdoUurWerker.IsChecked == true)
                    {
                        if (int.TryParse((txtAantal.Text), out aantal))
                        {
                            UurWerker uurWerker = new UurWerker(txtAchternaam.Text, txtVoornaam.Text, loon, aantal, geslacht);
                            VoegWerknemerToe(uurWerker);
                        }
                        else
                        {
                            throw new FormatException("Het aantal moet een geheel getal zijn!");
                        }
                    }
                    else if (rdoComissieWerker.IsChecked == true)
                    {
                        if (int.TryParse((txtAantal.Text), out aantal))
                        {
                            if (decimal.TryParse((txtCommissie.Text).Replace(".", ","), out commissie))
                            {
                                CommissieWerker commissieWerker = new CommissieWerker(txtAchternaam.Text, txtVoornaam.Text, loon, commissie, aantal, geslacht);
                                VoegWerknemerToe(commissieWerker);
                            }
                            else
                            {
                                throw new FormatException("De commissie moet een decimaal getal zijn!");
                            }
                        }
                        else
                        {
                            throw new FormatException("Het aantal moet een geheel getal zijn!");
                        }
                    }
                }
                else
                {
                    throw new FormatException("Het loon moet een decimaal getal zijn!");
                }
                txtAantal.Text = string.Empty;
                txtAchternaam.Text = string.Empty;
                txtVoornaam.Text = string.Empty;
                txtCommissie.Text = string.Empty;
                txtLoon.Text = string.Empty;
                RefreshListBox();
            }
            catch (FormatException fex)
            {
                MessageBox.Show(fex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Fout", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void RefreshListBox()
        {
            lbInhoud.ItemsSource = null;
            lbInhoud.ItemsSource = werknemers;
        }
        private void rdoComissieWerker_Checked(object sender, RoutedEventArgs e)
        {
            lblAantal.Visibility = Visibility.Visible;
            txtAantal.Visibility = Visibility.Visible;
            lblCommissie.Visibility = Visibility.Visible;
            txtCommissie.Visibility = Visibility.Visible;
        }
        private void rdoStukWerker_Checked(object sender, RoutedEventArgs e)
        {
            lblAantal.Visibility = Visibility.Visible;
            txtAantal.Visibility = Visibility.Visible;
            lblCommissie.Visibility = Visibility.Hidden;
            txtCommissie.Visibility = Visibility.Hidden;
        }
        private void rdoUurWerker_Checked(object sender, RoutedEventArgs e)
        {
            lblAantal.Visibility = Visibility.Visible;
            txtAantal.Visibility = Visibility.Visible;
            lblCommissie.Visibility = Visibility.Hidden;
            txtCommissie.Visibility = Visibility.Hidden;
        }
        private void Werknemer_Checked(object sender, RoutedEventArgs e)
        {
            lblAantal.Visibility = Visibility.Hidden;
            txtAantal.Visibility = Visibility.Hidden;
            lblCommissie.Visibility = Visibility.Hidden;
            txtCommissie.Visibility = Visibility.Hidden;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //rdoWerknemer.IsChecked = true;
            lblAantal.Visibility = Visibility.Hidden;
            txtAantal.Visibility = Visibility.Hidden;
            lblCommissie.Visibility = Visibility.Hidden;
            txtCommissie.Visibility = Visibility.Hidden;
        }
        private void VoegWerknemerToe(object werknemer)
        {
            bool bestaatAl = false;

            foreach (object x in werknemers)
            {
                if (werknemer.Equals(x))
                {
                    bestaatAl = true;
                }
            }

            if (bestaatAl)
            {
                throw new Exception("Deze werknemer bestaat al!");
            }
            else
            {
                werknemers.Add((Werknemer)werknemer);
            }

        }
    }
}
