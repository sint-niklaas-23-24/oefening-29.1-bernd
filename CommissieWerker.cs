﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Oefening_23._3
{
    internal class CommissieWerker : Werknemer
    {
        private int _aantal;
        private decimal _commissie;

        public CommissieWerker(string naam, string voornaam, decimal loon, decimal commissie, int aantal, BitmapImage geslacht) : base(naam, voornaam, loon, geslacht)
        {
            Aantal = aantal;
            Commissie = commissie;
        }
        public int Aantal 
        { 
            get { return _aantal; } 
            set 
            {
                if (value > 0)
                {
                    _aantal = value;
                }
                else
                {
                    throw new Exception("Het aantal kan niet kleiner of gelijk zijn aan 0!");
                }
            }
        }
        public decimal Commissie 
        { 
            get { return _commissie; } 
            set 
            {
                if (value >= 0)
                {
                    _commissie = value;
                }
                else
                {
                    throw new Exception("De commissie kan niet kleiner zijn dan 0!");
                }
            } 
        }
        public override decimal Verdiensten()
        {
            return Verdiensten() + (Aantal * Commissie);
        }
        public override string ToString()
        {
            return base.ToString() + " - CommissieWerker";
        }
    }
}
